# -*- coding: utf-8 -*-
from ..APIException import APIException
from kr.sdp.base.DBManager import DBManager

class Application(object):
    @classmethod
    def getVersion(cls):
        result = {"version":33,
                  "message":"수능 문제 채점이 가능합니다!\n최신 버전으로 업데이트 뒤 채점하세요!",
                  "market_url":"market://details?id=com.dulgi.MoGye&utm_medium=referral&utm_source=update&utm_campaign=33",
                  "force":False}

        return result