# -*- coding: utf-8 -*-
from ..APIException import APIException
from Application import Application


class ApplicationApi(object):
    def request(self, requestMethod, urlScheme, pathList, queryParamDict, postParamDict):
        try:
            depth1 = pathList.pop(0)    #/version
            depth2 = pathList.pop(0)    #/version/{app_id} => app_id
        except:
            raise APIException(1101)    #Invalid API

        if depth1 == "version":
            if depth2 == "iwillpass":
                return Application.getVersion()

        raise APIException(1101)    #Invalid API