# -*- coding: utf-8 -*-

import traceback

import urlparse
import ObjectController
#import ResultFormatter
import time
import time
import cStringIO
import os
import threading

import ResultFormatter 
from APIException import APIException

#from db.MySQLManager import MySQLManager 

class RestApiServer:
    
    OUTPUT_DEBUG = True
    
    StatusMsg = {}
    StatusMsg[200] = '200 OK'
    StatusMsg[304] = '304 Not Modified'
    StatusMsg[403] = '403 Forbidden'
    StatusMsg[404] = '404 Not Found'
    StatusMsg[500] = '500 Internal server error'
#    MySQLManager.connect()
    
    @classmethod
    def main(self, environ, start_response):        
        print 'main'
#        """
#        실질적으로 apache와 연결시 메인 함수   
#        """
        try :
            requestMethod =  environ['REQUEST_METHOD'].lower()    
            urlScheme = environ['wsgi.url_scheme']
            urlPath = environ['PATH_INFO']
            pathList = urlPath.split("/")[1:]
        
            queryParamDict = {}
            if environ.has_key('QUERY_STRING'):
                queryParamDict = dict( urlparse.parse_qsl(environ['QUERY_STRING']) )
            
        
            accessToken = None
            if queryParamDict.has_key('access_token'):
                accessToken = queryParamDict['access_token']
             
            postParamDict = {}
                
            try:
                length= int(environ.get('CONTENT_LENGTH', '0'))
            except:
                length = 0
                
            if length != 0 and environ['wsgi.input'] != None:
                postData = environ['wsgi.input'].read(length)
                postParamDict = dict(urlparse.parse_qsl(postData, keep_blank_values=True))
            
            if postParamDict.has_key('access_token'):
                accessToken = postParamDict['access_token']
            
            s = time.time()
            
            outputDict = ObjectController.main(requestMethod, urlScheme, pathList, queryParamDict, postParamDict, accessToken)
            print 'Exec_Time    %f' % (time.time() - s)
            
            output = ResultFormatter.OutputTrans(outputDict)
            
            status = self.StatusMsg[200]
            response_headers = [('Content-type', 'application/json'),
                                ('charset', 'utf-8'),
                                ('Content-Length', str(len(output)))]
            start_response(status, response_headers)
            return [output]
        
        
        ### API 에 정해진 형식이 아닌 경우 아래 Exception 에서 잡힌다. 
        except APIException as e:
            output = ResultFormatter.ErrTrans(e)
            
            status = self.StatusMsg[200]
            response_headers = [('Content-type', 'application/json'),
                                ('Content-Length', str(len(output)))]
            start_response(status, response_headers)
            
            return [output]
        
        ### 예기치 않는 오류가 발생하는 경우, 아래 코드가 타고  OUTPUT_DEBUG 가 True이면 
        ### traceback 형식의 로그가 보인다. 
        except:
            
            if self.OUTPUT_DEBUG == False:
                status = self.StatusMsg[505]
                output = ''
                response_headers = [('Content-type', 'text/html'),
                                ('Content-Length', str(len(output)))]
                start_response(status, response_headers)
            
                return [output]
            
            
            status = self.StatusMsg[500]
            output = traceback.format_exc()
            
            response_headers = [('Content-type', 'text/html'),
                                ('Content-Length', str(len(output)))]
            start_response(status, response_headers)
            
            return [output]
