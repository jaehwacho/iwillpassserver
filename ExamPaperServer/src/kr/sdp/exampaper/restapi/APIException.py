# -*- coding: utf-8 -*-
ErrorCode = {
#             # 1000 ~ 1099
             1001:'db query error',
             1002:'db insert error',
             1003:'db update error',
             1004:'db delete error',
             
             # 1100 ~ 1199
             1100:'Invalid Method(GET, PUT, POST, DELTE)',
             1101:'Invalid API',
             1102:'The input parameter error',
             
             5000:'Not Existed Channel Name',
             5001: 'Undefied Type',
                
             9999:'I have no idea Error'
             }

class APIException(Exception):
    def __init__(self, code, msg=None):
        self.code = code
        if msg is None:
            try:
                self.msg = ErrorCode[code]
            except:
                self.msg = 'unknown error code msg'
        else:
            self.msg = msg
    
    def getErrorCode(self):
        return self.code
    
    def getErrorMsg(self):
        return self.msg
