# -*- coding: utf-8 -*-


from exam.ExamApi import ExamApi
from application.ApplicationApi import ApplicationApi
from APIException import APIException

#서버 점검 여부
CHECK_SERVER = False

def main(requestMethod, urlScheme, pathList, queryParamDict, postParamDict, accessToken):
    if CHECK_SERVER == True:
        lang = None

    print '[ObjectController]path : ', pathList
    print '[ObjectController]query : ', queryParamDict
    currentPath = pathList[0]
    result = None
    
        
    if currentPath.startswith('exam'):
        api = ExamApi()
        result = api.request(requestMethod, urlScheme, pathList, queryParamDict, postParamDict)
    elif currentPath.startswith('version'):
        api = ApplicationApi()
        result = api.request(requestMethod, urlScheme, pathList, queryParamDict, postParamDict)
    else:
        raise APIException(1101)    #Invalid API
    return result
