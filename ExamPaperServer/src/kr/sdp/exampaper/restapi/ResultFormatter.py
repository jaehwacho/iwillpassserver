# -*- coding: utf-8 -*-

import json
#from Exception.APIException import APIException

def OutputTrans(resultDict, resultType = 'json'):
    outputDict = {}
    outputDict['result'] = resultDict
    outputDict['error'] = 0
    
    if resultType == 'json':

        output = json.dumps(outputDict)
        return output
    
def ErrTrans(exception, resultType = 'json'):
    outputDict = {}
    error_code = exception.getErrorCode()
    
    if error_code == 9001:
        outputDict['result'] = exception.getErrorMsg()
    else:
        outputDict['result'] = "NULL"
    
    outputDict['error'] = error_code
    
    if resultType == 'json':
        output = json.dumps(outputDict)
        return output