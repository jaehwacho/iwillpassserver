# -*- coding: utf-8 -*-
from ..APIException import APIException
import traceback
from datetime import datetime
from kr.sdp.base import DBManager 
import json
import mysql.connector

class Exam(object):
    #1. 시험지 목록 가져오기 API
    def getExamList(self):
        column_name = ['exam_id', 'exam_title', 'exam_start_time', 'exam_end_time', 'exam_total_point', 'exam_member_count']
        column_name_text = ','.join(column_name)
        connection = DBManager.getConnection()
        cursor = connection.cursor()
        result = {}
        try:
            query = "select %s from exam_information where available=1 ORDER BY exam_priority asc"%(column_name_text)
            cursor.execute(query)
            result_list = []
            row = cursor.fetchone()
            
            while row is not None:
                item = {}
                exam_id = -1;
                for index in range(len(column_name)):
                    if index == 0:
                        item[column_name[index]]="exam_"+ str(row[index])
                        exam_id = row[index]
                    elif index==2 or index==3:
                        item[column_name[index]] = datetime.strftime(row[index], '%Y-%m-%d')
                    else:
                        item[column_name[index]]=row[index]
                    
                    #set exam_member_count 
                    #TODO:성능 개선필요 (방법:Crontab활용)
                    try:
                        query = "SELECT count(*) FROM iwillpassdb.exam_user_answer where exam_id=%s"%(exam_id)
                        inner_connection = DBManager.getConnection()
                        inner_cursor = inner_connection.cursor()
                        inner_cursor.execute(query)
                        item['exam_member_count'] = inner_cursor.fetchone()[0]
                    except:
                        None
                        
                result_list.append(item)
                row = cursor.fetchone()
                
            result = {'data_count':len(result_list), 'data':result_list}
        except:
            print traceback.print_exc()
            raise APIException(1001)    #db query error
        finally:
            if cursor:
                cursor.close()                
            if connection:
                connection.close()
        return result        
        
    #2. 시험지 답 가져오기 API
    def getAnswer(self, exam_id):
        exam_id = exam_id.replace('exam_', '')
        column_name = ['question_id', 'answer', 'type', 'point']
        column_name_text = ','.join(column_name)
        connection = DBManager.getConnection()
        cursor = connection.cursor()
        result = {}
        try:
            query = "select %s from exam_answer where exam_id=%s ORDER BY question_id asc"%(column_name_text, exam_id)
            cursor.execute(query)
            result_list = []
            row = cursor.fetchone()
            total_point = 0
            while row is not None:
                item = {}
                for index in range(len(column_name)):
                    item[column_name[index]]=row[index]
                    if column_name[index] == 'point':
                        total_point += row[index]
                result_list.append(item)
                row = cursor.fetchone()
            result = {'data_count':len(result_list), 'data':result_list, 'total_point':total_point}
        except:
            print traceback.print_exc()
            raise APIException(1001)    #db query error
        finally:
            if cursor:
                cursor.close()                
            if connection:
                connection.close()
        return result
    
    #3.시험지 답 제출하기 API
    def updateUserAnswer(self, exam_id, user, answers, total_point):
        exam_id = exam_id.replace('exam_', '')
        id = user + '_' + exam_id
        needToUpdate = False
        #insert
        connection = DBManager.getConnection()
        cursor = connection.cursor()
        try:
            query = """INSERT INTO exam_user_answer(id, user_id, exam_id, user_answer, user_point) VALUES ('%s','%s',%s,'%s',%s)"""%(id, user, exam_id, answers, total_point)
            cursor.execute(query)
            connection.commit()
        except mysql.connector.IntegrityError:
            needToUpdate = True
        except:
            print traceback.print_exc()
            raise APIException(1002)    #DB insert error
        finally:
            if cursor:
                cursor.close()                
            if connection:
                connection.close()
               
        if needToUpdate is True:
            #update
            connection = DBManager.getConnection()
            cursor = connection.cursor()
            try:
                query = """UPDATE exam_user_answer SET user_answer='%s', user_point=%s WHERE id='%s' """%(answers, total_point, id)
                cursor.execute(query)
                connection.commit()
            except:
                print traceback.print_exc()
                raise APIException(1003)    #DB update error
            finally:
                if cursor:
                    cursor.close()                
                if connection:
                    connection.close()
        return True

