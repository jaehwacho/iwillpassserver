# -*- coding: utf-8 -*-
from ..APIException import APIException
from Exam import Exam
import json

class ExamApi(object):
    def request(self, requestMethod, urlScheme, pathList, queryParamDict, postParamDict):
        
        print '[ExamApi]pathList:', pathList
        print '[ExamApi]queryParamDict:', queryParamDict
        print '[ExamApi]postParamDict:', postParamDict
        
        try:
            depth1 = pathList.pop(0)    #/exam
            depth2 = str(pathList.pop(0))    #/exam/depth2
        except:
            raise APIException(1101)    #Invalid API
        
        if depth2 == 'list':
            if requestMethod == "get":
                #1. 시험지 목록 가져오기 API
                exam = Exam()
                return exam.getExamList()
            else:
                raise APIException(1100)    #Invalid Method(GET, PUT, POST, DELTE)
        elif depth2.startswith('exam_'):
            exam_id = depth2
            try:
                depth3 = pathList.pop(0)    #/exam/{exam아이디}/depth3
            except:
                raise APIException(1101)    #Invalid API
            if depth3 == "answer":
                if requestMethod == "get":
                    #2. 시험지 답 가져오기 API
                    exam = Exam()
                    return exam.getAnswer(exam_id)
                elif requestMethod == "put":
                    try:
                        user = postParamDict['user']
                        answers = postParamDict['answers']
                        total_point = postParamDict['total_point']
                        json.loads(answers)
                    except:
                        raise APIException(1102)    #The input parameter error
                    
                    #3.시험지 답 제출하기 API
                    exam = Exam()
                    return exam.updateUserAnswer(exam_id, user, answers, total_point)
                
                else:
                    raise APIException(1100)    #Invalid Method(GET, PUT, POST, DELTE)
            else:
                raise APIException(1101)    #Invalid API
        
        raise APIException(1101)    #Invalid API
        