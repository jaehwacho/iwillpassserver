# -*- coding: utf-8 -*-
import sys
import traceback

import os
import md5

error = None
try:
    PARENT_PATH = os.path.dirname(os.path.abspath(__file__)) + "/"
    sys.path.append(PARENT_PATH)
    sys.path.append(PARENT_PATH+'restapi')
    sys.path.append(PARENT_PATH+"../../../")
    from restapi.restApiServer import RestApiServer
except:
    error = traceback.format_exc()
    print error 
    pass


def WSGIHandler(environ, start_response):
    if error == None:
        return RestApiServer.main(environ, start_response)
    output = error
    response_headers = [ ('Content-type', 'text/html'), 
                        ('Content-Length', str(len(output)))]
    start_response('500 Internal server error', response_headers)
        
    return [output]

application = WSGIHandler

if __name__ == '__main__':
    """
         이 파일을 디버깅모드로 실행하고 브라우저에서 http://localhost:8822/ 접근하면 break point를 찍을 수 있음.  
    """
    
    # this runs when script is started directly from commandline
    try:
        application = WSGIHandler
        # create a simple WSGI server and run the application
        from wsgiref import simple_server
        print "Running test application - point your browser at http://localhost:8822/ ..."
        httpd = simple_server.WSGIServer(('', 8822), simple_server.WSGIRequestHandler)
        httpd.set_app(application)
        httpd.serve_forever()
    except ImportError:
        # wsgiref not installed, just output html to stdout
        for content in application({}, lambda status, headers: None):
            print content
           
