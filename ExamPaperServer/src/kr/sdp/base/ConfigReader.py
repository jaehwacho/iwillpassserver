# -*- coding: utf-8 -*-
import ConfigParser
class ConfigReader(object):
    config = ConfigParser.RawConfigParser()
    config.read("config.ini")
    
    @classmethod
    def get(cls, section, option):
        return cls.config.get(section, option)