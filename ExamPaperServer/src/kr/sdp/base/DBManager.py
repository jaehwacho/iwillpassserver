# -*- coding: utf-8 -*-
from DBUtils.PooledDB import PooledDB
import mysql.connector
from ConfigReader import ConfigReader

class DBManager(object):
    cofig_database = ConfigReader.get('DB', 'database')
    cofig_user = ConfigReader.get('DB', 'user')
    cofig_password = ConfigReader.get('DB', 'password')
    cofig_host = ConfigReader.get('DB', 'host')
    cofig_port = int(ConfigReader.get('DB', 'port'))
    cofig_poolsize = int(ConfigReader.get('DB', 'poolsize'))
    
    pool = PooledDB(mysql.connector, cofig_poolsize,
        database=cofig_database, user=cofig_user,password=cofig_password, host=cofig_host, port=cofig_port)
  
    @classmethod
    def getConnection(cls):
        try:
            connection = cls.pool.connection()
            return connection
        except Exception as e:
            print "[DBManager]Error:e"
            return None

    @classmethod
    def getDeticatedConnection(cls):
        try:
            connection = cls.pool.connection(shareable=False)
            return connection
        except Exception as e:
            print "[DBManager]Error:e"
            return None        